This is the LT telephone project.

**Contributors:**  
Prologue 1: Mag  
Prologue 2: RandomWizard  
Ch1: Mag  
Ch2: rainlash  
Ch3: Rasta  
Ch3x: Mag  
Ch3xx: RandomWizard  
Ch4: SirSpensir  
Ch5: Jumpants  
Ch6: RandomWizard  
Ch7: Had0uken  
Ch7x: SigmaRaven  
ch8: Lord_Tweed  
ch9: KD  
ch10: SigmaRaven  
ch11: OmegaX128  
Ch12: Cepheus  
Ch12x: Jumpants  
ch13: Lord_Tweed  
ch14: lielikelord  
Ch15: SirSpensir  
C16: OmegaX128  
C17: Jumpants  
C18: KD  
C19: Rasta  
C20/c/i/j: Mag  
Endgame/Epilogue: Had0uken  
Extra: Lord_Tweed  


## Credits  
From the FE Graphics Repo:  

**Portraits:**  
Kyuzeth_Ilus - ???  
Renny - Ezra  
TheWildCard OC1 - Ezra  
Vampyryte OC 1 - Ezra  
XVI_Alexandra - Ezra  
ZoramineFae_5 - Ezra  
PkMnBro1 - Hans - Mikhail (Old)  
Smokeyguy77_1 - ???  
Lenh - Kingfisher  
1(XPGamesNL) - ???  
Ambrosiac - Master Cultist
Atey(BorsDeep) Travant - Magister
rainlash - Yarrow, Thrant, Ophie, Nialla
rainlash, Kanna - Finola
rainlash, KD - Fessian
XVI - Armorer, Edward  
Zarg - Ezra  
Laurent_lacroix - Sionn, Pitta, Buyer, Isador, Goibniu, Rayner  
Flasuban - Cistia, Shoebill  
Can Dy - Accipiter, Ennis 2  
Kanna - Ayla, Caer, Striga
Generic Pretsel - Osbert, Knight, Clyde  
Goldblitzx - Prince Zackary  
ZessDynamite - Abeni, Ennis, Subira  
Sweet Basil - Lady Con  
lmr - Dullahan  
L95 - Mogall, Lignabaste  
caringcarrot - Young Shoebill  
Melia - Lim, Amada, Falcon  
RandomWizard - Bronach, Sadhbh, Moore, Cormorant, Kevin, Owl, Tenebrae, Kalash, Remri, Fessian, Brandon, Ayla  
Sdaht, LaurentLacroix - Lychee  
RandomWizard, MeatOfJustice - Farl  
Lenh - Steel Fox  
JeyTheCount - Cook, Truk  
Knabepicer - Ysbryd, Maeve  
WAve - Tiberius, Aengus  
Glaceo, Mr_Karkino - Female O'Neill  
HyperGammaSpaces - Villagers  
KD - Brandon, Remri  
Cravat - Bluejay  
Its_Just_Jay - Faviyn, Erskine  
DerTheVaporeon - Luan  
Klokinator - Jarze, Seliche  
Had0uken - Tenebrae, Owl (old recolor)  
Sphealnuke - Byrne  
Tchyrimi_2 - Claudius  
CanDy F2E 9 - Domina  
(JeyTheCount) Nomad Generic 1 - Killer  
Levin64 - Pipit, Pityria
CapibarainSpace - Quelea, Isador Dream Guy  
MeatOfJustice, Melia - Junco  
Nickt - Fiachra  
Blade - Barra  
SSHX - Vengarde King, Lovebird (edited)  
Zeldacrafter - Rachel  
Eldritch Abomination - Hugo  
Toa - Biran, Kalash  
Roze - Kalash  
Raymond - Young Connachta  
SeaLion - Enya  
JiroPaiPai - Sparrow
Laurent - Gospel
Ghostblade513 - Biddy
SterlingGlovner - Balhkoin  
Ennis Suzerain - LittleKyng


**Animations:**
Serif and FE7if devs (Hector Milia)  
RedBean (Celica)  
Nuramon (Heavy Infantry, Gold Knight animation)  
SALVAGED (Cavalier M and F, Knight, Paladin M and F)  
Flasuban (Improved Falcoknight, Pegasi T1 repal v2 + Weapons)  
SixThousandHulls (Pegasi T1 M)  
Leo_link(Fighter, Ranger)  
skitty (Merceneary Repalette)  
More Eprhaim animations by DerTheVaporeon, Pikmin1211.  
Cath Repal by Pikmin  
Fancy Bard by MeatOfJustice  
Ephraim Great Lord animations by eCut, Belle, and St.Jack  
Rogue anims by Orihara_saki, Ukulele, SD9k  
Fae to Myrrh Dragon by Teraspark  
Axe Wyvern Rider - Mikey Seregon, Alfred Kamon  
Female Brigand (Barbarian) - eCut, Skitty  
Female Berserker (Armored) v2 Repal - eCut, Skitty, Serif  
Leo-Style +Weapons Ranger - Leo_Link, SALVAGED, knabepicer, Jey the Count  
Deacon v2 Repal +Weapons - Pikmin1211, GabrielKnight, Maiser6, Lisandra_Brave, TBA  
Female Pupil - Pikmin1211  
Male Thief Repallete - Pikmin1211  
Mercenary Lord & NYZGamer - Dragoon  
Nuramon - Void Dragon  
Shapeshifter - Leo_Link (Trickster)  
BatimaTheBat(Mage Knight F Repalette)  
seergiioo - Valkyrie  
WyvernKnight Repal +Weapons - Fewa, Lord_Tweed, St Jack, Itranc  
Spy (Ninja M) - Pikmin1211, DerTheVaporeon  
Dancer - Circleseverywhere, Kao  
Sentinel (Spartan) - Pikmin1211, Nuramon, Vilk  
Pirate (Sword) - Pimpstick, Maiser6  
Griffon Rider - Ayer  
DemonKing - IS, SHYUTERz, Had0uken(LT color edit)  
Warrior Repalette - Pushwall  
SkidMarc25 - Lich  
Neph - Pugilist  
St Jack - Master Lord  
Adventurer - ltranc, Nuramon  
Warrior F - Temp, Pushwall  
Archsage Lilina - Redbean  
Wild Wyvern - Sme  
Fighter-Type [M] Tellius-Style Bow-Only - Leo_Link, knabepicer, Pushwall  
Roxy [F] Light Mage - L95  
Corsair (Dart Style Berserker) - Greentea, Spud, BwdYeti, Raspberry, DerTheVaporeon

**Maps:**  
ZoramineFae  
N246  
LordGlenn  
Swamp tileset - Hypergammaspaces  
Frontier tileset - Flasuban, WAve, ZoramineFae  
Lava Fields tileset - Hypergammaspaces, cecilyn  
Snow Castle - Peerless  
Snowy Bern - FEAW, ZoramineFae, Vennobennu  
Chapter 10 - Apparoid, Zoramine, Flasuban, WAve, N426  
Castle Pandemonium - WAve  
Castle Green 2 tileset - FEAWS  
FE7 Castle colortiles edit - Had0uken
Updated villege tileset -  N426, Zoramine, and Venno  
Ch 15:Isador's dream -Lord Glenn  
Desert Bastion Tileset - Hypergammaspaces  
C17 map - Zoramine Fae (edited)  

**Icons:**
EldritchA, Indogutsu Tenbuki - Tactics Ogre Sheets  
Mag, NerysRhys - FFTA2 Sheets  
Mag, Kamon, OmniGamerX - Kirby Food Sprites  
Ershkigal - Dagger Sprites, Razorwind, Dire Thunder, Manablast, Sear, Holy Arrows  
XVI  
LisandraBrave  
GabrielKnight  
Rasta - Knife wexp icon  
Celice - Ballista weapon icons  
2WB - Member Card  
Beansy - Bolt Axe, Fish, Rupture, Encroach, Conviction, Annihilation, Purgatory, Lances  
Jeorge_Reds - Hit and Run  
Zane - Deviant Fragarach  
ZessDynamite - Food Icons  

**Map Sprites:**  
TBA Halberdier  
Red Bean Celica  
Shin19 F!Hector  
Fancy Bard by MeatOfJustice  
t2 Ephriam Map Anims by Sme, milom  
t2 Osbert sprite by Der, Pikmin  
t1 Ephraim Map anims by SSHX, Zoramine Fae, Tetraspark  
Female Brigand anims by Skitty  
Female Mercenary anims by Agro  
Female Fighter anims by Alusq  
Female Warrior anims by FEGirls  
Knight (U) Axe by Agro  
Dragon (U) Myrrh Generic by Kemui52  
Wyvern Rider Axe by flasuban  
Brigand (F) by Skitty  
Berserker (F) by Unknown  
Valkyrie (M) Non-Religious by Der  
Marksman by ArcherBias  
Swordmaster Reskin F by Russel Clark  
L95 - Void Dragon, Ice Dragon  
Airship by 2daemoth, ZarG, 7743  
Assasin (F) by RobertFPY  
Bramimond by IS  
Trickster by StreetHero,blood  
Dracozombie by IS  
Demon King (Improved) by MrGreen, IS  
Spy (Ninja M) - DerTheVaporeon  
Sentinel (Spartan) - DerTheVaporeon  
Griffon Rider - N426  
Lich - SkidMarc25  
Master Knight - repo, uncredited  
Magic Turret - CardCafe  
Crates - PrimeFusion  
Fighter (M) Bow - {Scraiza}  
Deacon (M) (Male Troubadour) - Unknown
Sword Cavalier - Agro
Corsair (Dartzerker) - WarPath

**Class Cards:**  
Knight (U) Axe by Der, Epicer  
Spy (Ninja M) - DerTheVaporeon  
Sentinel (Spartan) - DerTheVaporeon  
Griffon Rider - EldritchAbomination, N426, Jj09  

**Music:**  
dragstondev (FE5 cover)  
Christopher Larkin  
Motoi Sakuraba  
Orange Free Sounds  
Hans Zimmer - He's A Pirate  
Hitoshi Sakimoto and Masaharu Iwata  
Ibizu (Circle Sanbondo) - Dreaming Beauty, Lunatic Guardian  
Yoko Shimomura - Final Fantasy XV  
Yoko Shimomura, Yoshitaka Suzuki - FFXV Veiled in Black -Insomnia Arrangement-  
Tadayoshi Makino - FFXV A Daunting Challenge  
Nobuo Uematsu - Granblue Fantasy  
Tsutomu Narita - Granblue Fantasy Omega-3 Battle, Granblue Fantasy Dragon's Circle -Light and Dark, GBFV Peacemaker's Wings  
Yoshito Sekigawa - Fire Emblem  
Atsushi Yoshida - Fire Emblem  
The End of Raging Winds (Etrian Mystery Dungeon Ver.) - Yuzo Koshiro, Takeshi Yanagawa, Kazune Ogihara  
Each Their Own Justice (Etrian Mystery Dungeon Ver.) - Yuzo Koshiro, Takeshi Yanagawa(?), Kazune Ogihara  
Olberic, the Warrior - Yasunori Nishiki  
Masashi Hamauzu - Sigma Harmonics  
Pedro Silva - Omori  
Andriesh Gandrabu - Pathlogic  
Yoh Ohyama - Mary Skelter  
Masaru Yokoyama - Sirus the Jager  
Hiroki Morishita- Dusk Falls(Fire Emblem Fates) - Chapter 3 map theme  
Beautiful Dead - Masafumi Takada  
New World Order - Masafumi Takada  
Time to Fight! (Bionis' Shoulder) - Kenji Hiramatsu  
Breakthrough.exe - Hayato Asano  
TIGAR -Kurt - Hayato Asano  
Lucky Lucky Lives - Takumi Ozawa  
Battle Against Magius -Takumi Ozawa  
Rumor Theater - Takumi Ozawa  
Magica record event challenge - Naoki Chiba  
Settlement in the Red Bluffs -Yasunori Nishiki  
hierophant's palace - Akira Senju  
Welcome one and all -Akira Senju  
Aristocrats - Akira Senju  
Daughter of Hollownest - Christopher Larkin  
Camellia - Kuniyuki Takahashi  
Ameno (2010) - ERA  
796f756b6e6f7765766572797468696e67 - Keisuke Ito  
Semi-final Battle - Tsukasa Tawada  
Queen's Garden - Hollow Knight  
Infinite Sadness, Intensity of Will, A Doll's Sadness - Fate Kaleid Liner Prisma Illya: Licht (Namae no nai Shoujo)  
Hiroyuki Sawano (NzK) - On:9Re:Eita-Zu  
Go Sakabe - Rhapsody Rage  
His World (GBA Remix) - TLP Team
Captive Heart, Truthbearer's Observance, What We've Built - Blue Reflection OST - Hayato Asano 



**Backgrounds:**
Eldritch Abomination  
Majin Tensei  
Space - Granblue Fantasy  
CastlePlanet - IS, Had0uken (edit)  
Throne Room, Caravan 2 Night, Castle Interior 1/5/6 Night, Outside Castle Night, Inside House 2 Nighttime, Ruins 2 - Fire Emblem: Path of Radiance/Fire Emblem: Radiant Dawn by Intelligent Systems  

**Misc assets:**  
Temporal Tower assets from Pokemon Explorers of Time  

ZeroXA

Ennis Portrait Ayla Ending - Fortuna