from __future__ import annotations

from app.data.database.components import ComponentType
from app.data.database.skill_components import SkillComponent, SkillTags
from app.engine import (action, banner, combat_calcs, engine, equations,
                        image_mods, item_funcs, item_system, skill_system,
                        target_system)
from app.utilities import (static_random)
from app.engine.game_state import game
from app.engine.objects.unit import UnitObject
from app.engine.combat import playback as pb
from app.utilities.enums import Strike
import logging

class DoNothing(SkillComponent):
    nid = 'do_nothing'
    desc = 'does nothing'
    tag = SkillTags.CUSTOM

    expose = ComponentType.Int
    value = 1

class EventAfterCombat(SkillComponent):
    nid = 'event_after_combat'
    desc = 'Calls event after any combat'
    tag = SkillTags.ADVANCED

    expose = ComponentType.Event
    value = ''

    def end_combat(self, playback, unit: UnitObject, item, target: UnitObject, item2, mode):
        game.events.trigger_specific_event(self.value, unit, target, unit.position, {'item': item, 'item2': item2, 'mode': mode})

class GainSkillAfterAnyCombat(SkillComponent):
    nid = 'gain_skill_after_combat'
    desc = "Gives a skill to user after any combat"
    tag = SkillTags.COMBAT2

    expose = ComponentType.Skill

    def end_combat(self, playback, unit, item, target, item2, mode):
        if  target and unit.team != target.team:
            action.do(action.AddSkill(unit, self.value))
            action.do(action.TriggerCharge(unit, self.skill))

class UpkeepEvent(SkillComponent):
    nid = 'upkeep_event'
    desc = "Triggers the designated event at upkeep"
    tag = SkillTags.TIME

    expose = ComponentType.Event
    value = ''

    def on_upkeep(self, actions, playback, unit):
        game.events.trigger_specific_event(self.value, unit, unit, unit.position, {'item': None, 'mode': None})

class EventBeforeCombat(SkillComponent):
    nid = 'event_before_combat'
    desc = 'Calls event before any combat'
    tag = SkillTags.ADVANCED

    expose = ComponentType.Event
    value = ''

    def start_combat(self, playback, unit: UnitObject, item, target: UnitObject, item2, mode):
        game.events.trigger_specific_event(self.value, unit, target, unit.position, {'item': item, 'item2': item2, 'mode': mode})

class SelfNihil(SkillComponent):
    nid = 'self_nihil'
    desc = "Skill does not work if the unit has this other skill"
    tag = SkillTags.CUSTOM

    expose = (ComponentType.List, ComponentType.Skill)
    value = []

    ignore_conditional = True

    def condition(self, unit, item):
        all_target_nihils = set(self.value)
        for skill in unit.skills:
          if skill.nid in all_target_nihils:
            return False
        return True

class CannotUseNonMagicItems(SkillComponent):
    nid = 'cannot_use_non_magic_items'
    desc = "Unit cannot use or equip non-magic items"
    tag = SkillTags.BASE

    def available(self, unit, item) -> bool:
        return item_funcs.is_magic(unit, item)

class EvalRegeneration(SkillComponent):
    nid = 'eval_regeneration'
    desc = "Unit restores HP at beginning of turn, based on the given evaluation"
    tag = SkillTags.CUSTOM

    expose = ComponentType.String

    def on_upkeep(self, actions, playback, unit):
        max_hp = equations.parser.hitpoints(unit)
        if unit.get_hp() < max_hp:
            from app.engine import evaluate
            try:
                hp_change = int(evaluate.evaluate(self.value, unit))
            except:
                logging.error("Couldn't evaluate %s conditional" % self.value)
                hp_change = 0
            actions.append(action.ChangeHP(unit, hp_change))
            # Playback
            playback.append(pb.HitSound('MapHeal'))
            playback.append(pb.DamageNumbers(unit, -hp_change))
            if hp_change >= 30:
                name = 'MapBigHealTrans'
            elif hp_change >= 15:
                name = 'MapMediumHealTrans'
            else:
                name = 'MapSmallHealTrans'
            playback.append(pb.CastAnim(name))

class EternalBlazeDamage(SkillComponent):
    nid = 'eternal_blaze_damage'
    desc = "Dynamically sets damage to equal the condition at the beginning of combat"
    tag = SkillTags.DYNAMIC

    expose = ComponentType.String
    value = '0'

    _evaluated_value = 0

    def dynamic_damage(self, unit, item, target, item2, mode, attack_info, base_value) -> int:
        return self._evaluated_value

    def pre_combat(self, playback, unit, item, target, item2, mode):
        from app.engine import evaluate
        try:
            x = evaluate.evaluate(self.value, unit, target, unit.position, {'item': item, 'item2': item2, 'mode': mode})
            self._evaluated_value = x
        except Exception as e:
            print("%s: Could not evaluate combat condition %s" % (e, self.value))

    def test_on(self, playback, unit, item, target, item2, mode):
        self.pre_combat(playback, unit, item, target, item2, mode)

class SelfRecoil(SkillComponent):
    nid = 'self_recoil'
    desc = "Unit takes non-lethal damage after any combat"
    tag = SkillTags.CUSTOM

    expose = ComponentType.Int
    value = 0
    author = 'Lord_Tweed'

    def end_combat(self, playback, unit, item, target, item2, mode):
        if target:
            end_health = unit.get_hp() - self.value
            action.do(action.SetHP(unit, max(1, end_health)))
            action.do(action.TriggerCharge(unit, self.skill))

class EvalGaleforce(SkillComponent):
    nid = 'eval_galeforce'
    desc = "Unit can move again if conditions are met. Value must resolve to a Boolean."
    tag = SkillTags.CUSTOM
    
    expose = ComponentType.String
    value = ''
    author = 'Lord_Tweed'

    def end_combat(self, playback, unit, item, target, item2, mode):
        from app.engine import evaluate
        try:
            x = bool(evaluate.evaluate(self.value, unit, target, unit.position, {'item': item, 'item2': item2, 'mode': mode}))
            if x:
                action.do(action.Reset(unit))
                action.do(action.TriggerCharge(unit, self.skill))
        except Exception as e:
            print("%s: Could not evaluate EvalGaleforce condition %s" % (e, self.value))

class LoseSkillAfterAnyAttack(SkillComponent):
    nid = 'lose_skill_after_any_attack'
    desc = "This skill is removed from user after an attack during any phase"
    tag = SkillTags.CUSTOM
    
    author = 'Lord_Tweed'

    def end_combat(self, playback, unit, item, target, item2, mode):
        mark_playbacks = [p for p in playback if p.nid in ('mark_miss', 'mark_hit', 'mark_crit')]
        if any(p.attacker is unit for p in mark_playbacks):  # Unit attacked
            action.do(action.RemoveSkill(unit, self.skill))

class LostOnTakeHit(SkillComponent):
    nid = 'lost_on_take_hit'
    desc = "This skill is lost when receiving an attack"
    tag = SkillTags.CUSTOM
    
    author = 'Lord_Tweed'

    def after_take_strike(self, actions, playback, unit, item, target, item2, mode, attack_info, strike):
        action.do(action.RemoveSkill(unit, self.skill))

class ArmsthriftWeps(SkillComponent):
    nid = 'armsthrift_weps'
    desc = 'Prevents degradation of weapons and spells only.'
    tag = SkillTags.CUSTOM

    expose = ComponentType.Int
    value = 1

    def _after_strike(self, actions, unit, item):
        if item_system.unrepairable(unit, item) or not (item_system.is_weapon(unit, item) or item_system.is_spell(unit, item)):
            return  # Don't restore for unrepairable items, or a non-weapon or spell
        # Handles Uses
        if item.data.get('uses', None) and item.data.get('starting_uses', None):
            curr_uses = item.data.get('uses')
            max_uses = item.data.get('starting_uses')
            actions.append(action.SetObjData(item, 'uses', min(curr_uses + self.value - 1, max_uses)))
        # Handles Chapter Uses
        if item.data.get('c_uses', None) and item.data.get('starting_c_uses', None):
            curr_uses = item.data.get('c_uses')
            max_uses = item.data.get('starting_c_uses')
            actions.append(action.SetObjData(item, 'c_uses', min(curr_uses + self.value - 1, max_uses)))

    def after_strike(self, actions, playback, unit, item, target, item2, mode, attack_info, strike):
        if not item:
            return

        if item.parent_item:
            self.after_strike(actions, playback, unit, item.parent_item, target, item2, mode, attack_info, strike)
        if strike != Strike.MISS or (item.uses_options and item.uses_options.lose_uses_on_miss()):
            self._after_strike(actions, unit, item)